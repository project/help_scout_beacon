<?php

namespace Drupal\help_scout_beacon\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class HelpScoutBeaconSettingsForm.
 *
 * The config form for the Help Scout Beacon module.
 *
 * @package Drupal\help_scout_beacon\Form
 */
class HelpScoutBeaconSettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'help_scout_beacon_settings';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'help_scout_beacon.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('help_scout_beacon.settings');

    $form['help_scout_beacon_form_id'] = [
      '#type' => 'textfield',
      '#title' => t('Beacon form id'),
      '#description' => t('Please enter your Help Scout form id, you can obtain this following: http://docs.helpscout.net/article/539-working-with-beacon#creating'),
      '#default_value' => $config->get('help_scout_beacon_form_id', NULL),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('help_scout_beacon.settings')
      ->set('help_scout_beacon_form_id', $values['help_scout_beacon_form_id'])
      ->save();
  }

}
