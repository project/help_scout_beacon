-- SUMMARY --

This module provides very basic integration with Help Scout's Beacon
(https://www.helpscout.net) - A helpdesk tool. Allowing people to submit
support tickets from your Drupal site or chat.

-- REQUIREMENTS --

No Drupal requirements other than core, but you'll need to create
an account in Help Scout:

https://docs.helpscout.com/article/1250-beacon-jumpstart-guide#creating


-- INSTALLATION --

1. Enable the module.

2. Click the configure link on the modules list or navigate
to http://yoursite.com/admin/config/services/help-scout-beacon/settings

3. Enter your form id and click save.

4. Navigate to Drupal's permission pages and set the required permissions.

-- CUSTOMIZATION --

None
