/**
  * This code is mostly from Help Scout, as documented here: http://docs.helpscout.net/article/539-working-with-beacon#creating
  */
!function (e, o, n) {
  // Check the variable is been passed.
  Drupal.behaviors.help_scout_beacon = {};
  Drupal.behaviors.help_scout_beacon.attach = function() {
    if (Drupal.settings && Drupal.settings.help_scout_beacon) {

      // Set the variable passed form the module.
      var help_scout_beacon_form_id_data = Drupal.settings.help_scout_beacon.help_scout_beacon_form_id_key;

      window.HSCW = o, window.HS = n, n.beacon = n.beacon || {};
      var t = n.beacon;
      t.userConfig = {}, t.readyQueue = [], t.config = function(e) {
        this.userConfig = e
      }, t.ready = function(e) {
        this.readyQueue.push(e)
      }, o.config = {
        docs: {
          enabled: !1,
          baseUrl: ""
        },
        contact: {
          enabled: !0,
          formId: help_scout_beacon_form_id_data
        }
      };
      var r = e.getElementsByTagName("script")[0],
        c = e.createElement("script");
      c.type = "text/javascript", c.async = !0, c.src = "https://djtflbt20bdde.cloudfront.net/", r.parentNode.insertBefore(c, r)
    }
  }
}(document, window.HSCW || {}, window.HS || {});
